let top_side_2 = document.querySelector('.sayd_b')
let imagesS = document.querySelectorAll('.sayd_b div')
let counter_5 = 0
let status = true

const NEXT_SLIDEA = (isNext) => {
    // Удаляем класс у всех элементов
    for (let item of imagesS) {
        item.style.display = 'none'
    }

    if (isNext) {
        counter_5++

        // Включаем бесконечную прокрутку
        if (counter_5 > imagesS.length - 1) {
            // Убрать класс у последнего
            counter_5 = 0
        }
    } else {
        counter_5--

        if (counter_5 < 0) counter_5 = imagesS.length - 1
    }

    imagesS[counter_5].style.display = 'block'
}

let slider_2 = setInterval(() => {
    if (status === true) {
        NEXT_SLIDEA(true)
    }
}, 5000)

// Pause slider
top_side_2.onmouseenter = () => status = false

// Continue slider
top_side_2.onmouseleave = () => status = true

//////////////////////////////////////////////////////////

let top_side = document.querySelector('.slaider')
let im = document.querySelectorAll('.slaider_block')
let counter__1 = 0
let status_a = true


let dots = document.querySelector(".dots")
let dots_count = 0
for (let item of im) {
    let div = document.createElement("div")
    let dot_num = dots_count++
    // if(dot_num >= im.length) dot_num =   
    div.setAttribute('count', dot_num)

    div.onclick = () => {
        // for (let i of im) {
        //     i.classList.remove('active_slaider')
        // }
        console.log(dot_num);
        counter__1 = div.getAttribute('count', dot_num)

        for (let el of document.querySelectorAll(".dots div")) {
            el.classList.remove('active_dots')
        }
        div.classList.add('active_dots')

        for (let els of im) {
            els.classList.remove("active_slaider")
        }
        im[dot_num].classList.add("active_slaider")

        // div.classList.add("active_slaider")
    }

    dots.append(div)
}

const NEXT_SLIDE_S = (isNext) => {
    if (isNext) {
        // Включаем бесконечную прокрутку
        if (counter__1 >= im.length - 1) {
            // Убрать класс у последнего
            counter__1 = 0
            for (let item of im) {
                item.classList.remove("active_slaider")
            }
            for (let item of document.querySelectorAll(".dots div")) {
                item.classList.remove("active_dots")
            }
            document.querySelectorAll(".dots div")[counter__1].classList.add("active_dots")
            im[counter__1].classList.add("active_slaider")
        } else {
            for (let item of im) {
                item.classList.remove("active_slaider")
            }
            for (let item of document.querySelectorAll(".dots div")) {
                item.classList.remove("active_dots")
            }
            counter__1++
            im[counter__1].classList.add("active_slaider")

            document.querySelectorAll(".dots div")[counter__1].classList.add("active_dots")
        }
    } else {
        counter__1--

        if (counter__1 < 0) counter__1 = im.length - 1
    }

}

let slider_1 = setInterval(() => {
    if (status_a === true) {
        NEXT_SLIDE_S(true)
    }
}, 5000)

// Pause slider
top_side.onmouseenter = () => status_a = false

// Continue slider
top_side.onmouseleave = () => status_a = true

let prev = document.querySelector(".prev")
let next = document.querySelector(".next")

// Переключение слайдов вправо
prev.onclick = () => {
    NEXT_SLIDE_S(false)
}

// Переключение слайдов влево
next.onclick = () => {
    NEXT_SLIDE_S(true)
}
