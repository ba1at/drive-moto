let getingArr = [JSON.parse(localStorage.getItem('arr'))],
    puti = document.querySelector('.puti span'),
    img__tovat = document.querySelector(".img__tovat"),
    left = document.querySelector('.left'),
    price = document.querySelector('.price'),
    skidka = document.querySelector('.skidka'),
    nameOf_product = document.querySelector('.name_product_2'),
    kod = document.querySelector('.kod span'),
    stars = document.querySelector('.stars'),
    starsReiting = document.querySelectorAll('.stars div'),
    allForjs = document.querySelectorAll('.xarakteristika .bottom div span'),
    showXarakteristik = document.querySelector('.space_b center p'),
    centerP = document.querySelector('center[name="p"]'),
    search_img = document.querySelector('.search');

puti.innerHTML = localStorage.getItem('text') + '>' + JSON.parse(localStorage.getItem('arr')).name


for (let item of getingArr) {
    img__tovat.src = item.img
    nameOf_product.innerText = item.name
    kod.innerText = ' ' + item.cod
    allForjs[0].innerText = item.country
    allForjs[1].innerText = item.count
    allForjs[2].innerText = item.power_l_s
    allForjs[3].innerText = item.maks_km_h
    allForjs[4].innerText = 'Бензиновый'
    allForjs[5].innerText = item.year
    allForjs[6].innerText = item.brend
    search_img.src = './img/lupa.svg'

    if (item.count <= 0) {
        price.innerHTML = 'нет в наличии'
    } else {
        price.innerHTML = item.price + '₽'
    }

    for (let el of starsReiting) {
        el.innerHTML = `<svg width="23" height="21" viewBox="0 0 23 21" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M11.5375 0L14.1278 7.9463H22.5103L15.7287 12.8574L18.3191 20.8037L11.5375 15.8926L4.75593 20.8037L7.34626 12.8574L0.564686 7.9463H8.94717L11.5375 0Z" fill="#C4C4C4"/>
            </svg>
            `
    }

    for (let index = 0; index <= item.star - 1; index++) {
        starsReiting[index].innerHTML = `<svg width="23" height="21" viewBox="0 0 23 21" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11.5375 0L14.1278 7.9463H22.5103L15.7287 12.8574L18.3191 20.8037L11.5375 15.8926L4.75593 20.8037L7.34626 12.8574L0.564686 7.9463H8.94717L11.5375 0Z" fill="#1C62CD"/>
        </svg>`
    }


    if (item.sale == true) {
        let img = document.createElement('img')

        img.classList.add('sale')

        img.src = './img/sale_img.svg'
        if (item.count <= 0) {
            skidka.innerText = ''
        } else {
            skidka.innerText = item.price + '₽'
        }

        left.append(img)
    }
}

showXarakteristik.onclick = () => {
    if (showXarakteristik.hasAttribute('active')) {
        showXarakteristik.children[0].style.transform = 'rotate(0deg)'

        showXarakteristik.parentNode.style.height = '22px'

        showXarakteristik.removeAttribute('active')
    } else {
        showXarakteristik.children[0].style.transform = 'rotate(180deg)'

        showXarakteristik.parentNode.style.height = 'auto'

        showXarakteristik.setAttribute('active', true)
    }
}

centerP.onclick = () => {
    if (centerP.hasAttribute('active')) {
        centerP.parentNode.style.height = '335px'
        centerP.children[1].style.transform = 'rotate(90deg)'
        centerP.children[1].style.right = '-5px'

        centerP.removeAttribute('active')
    } else {
        centerP.parentNode.style.height = 'auto'
        centerP.children[1].style.transform = 'rotate(270deg)'
        centerP.children[1].style.right = '-20px'

        centerP.setAttribute('active', true)
    }
}

const RELOAD_POPULAR = (elem, append) => {
    let block = document.createElement('div')
    let img_sale = document.createElement('img')
    let like = document.createElement('img')
    let img = document.createElement('img')
    let name_product = document.createElement('p')
    let price = document.createElement('p')
    let kart = document.createElement('img')
    let btn = document.createElement('button')

    block.setAttribute('id', elem.id)
    block.setAttribute('count', elem.count)
    block.setAttribute('data', elem.year)

    block.classList.add('popular_block')
    img_sale.classList.add('img_sale')
    like.classList.add('like_block')
    img.classList.add('img_tavar')
    name_product.classList.add('name_product')
    price.classList.add('price_block')
    kart.classList.add('kart_img')
    btn.classList.add('btvac')

    let big = elem.max

    if (elem.max == true) {
        block.setAttribute('its_max', true)
    } else { }

    img_sale.src = './img/sale_img.svg'
    like.src = './img/like.svg'
    img.src = elem.img
    name_product.innerText = elem.name
    kart.src = './img/kart_img.svg'
    btn.innerText = 'посмотреть товар'

    block.append(img_sale, like, img, name_product, price, kart, btn)
    append.append(block)

    if (elem.sale == true) {
        img_sale.style.display = 'block'
        block.setAttribute('salle', true)
    } else {
        img_sale.style.display = 'none'
    }

    if (elem.count == 0) {
        price.innerText = 'нет в наличии'
        price.style.fontSize = '20px'
        price.style.marginTop = '0px'
    } else {
        price.innerText = elem.price
    }

    btn.onclick = () => {
        id(btn.parentNode.id, 0)
    }
}



let category___selection_p = document.querySelectorAll('.category___selection p')
let if_none = document.querySelector('.if_none')

for (let i of category___selection_p) {
    i.onclick = () => {
        for (let item of category___selection_p) {
            item.classList.remove('active_category_selection')
        }
        i.classList.add('active_category_selection')
        if (anasynk[i.getAttribute('hello_wepro')].length <= 1) {
            if_none.style.display = 'flex'
            res_categoria_2.style.display = 'none'
        } else {
            if_none.style.display = 'none'
            res_categoria_2.style.display = 'flex'
            res_categoria_2.innerHTML = ''
            for (let el of anasynk[i.getAttribute('hello_wepro')]) {
                RELOAD_POPULAR(el, res_categoria_2)
                scrol()

                let blocks = document.querySelectorAll('.popular_block .btvac')

                for (let u of blocks) {
                    u.onclick = () => {
                        id(u.parentNode.id, i.getAttribute('hello_wepro'))
                    }
                }
            }
        }
    }
}

let res_categoria_2 = document.querySelector('.res_categoria_2')

for (let item of anasynk[0]) {
    RELOAD_POPULAR(item, res_categoria_2)
}

const scrol = () => {
    let images = document.querySelectorAll('.res_categoria_2 div')
    for (let item of images) {
        item.style.display = 'none'
        images[0].style.display = 'block'
    }
    let top_side_2 = document.querySelector('.res_categoria_2')
    let counter = 0

    let status = true

    const NEXT_SLIDE = (isNext) => {
        // Удаляем класс у всех элементов
        for (let item of images) {
            item.style.display = 'none'
        }

        if (isNext) {
            counter++

            // Включаем бесконечную прокрутку
            if (counter >= images.length) {
                // Убрать класс у последнего
                counter = 0
            }
        } else {
            counter--

            if (counter < 0) counter = images.length - 1
        }

        images[counter].style.display = 'block'
    }

    let slider = setInterval(() => {
        if (status === true) {
            NEXT_SLIDE(true)
        }
    }, 4000)

    // Pause slider
    top_side_2.onmouseenter = () => status = false

    // Continue slider
    top_side_2.onmouseleave = () => status = true
}
scrol()

function id(id, sec) {
    let find = anasynk[sec].filter(item => item.id == id)[0]
    localStorage.setItem('arr', JSON.stringify(find))
    window.location.href = './elem.html'
}

function magnify(imgID, zoom) {
    var img, glass, w, h, bw;
    img = document.getElementById(imgID);

    /* Создать увеличительное стекло: */
    glass = document.createElement("DIV");
    glass.setAttribute("class", "img-magnifier-glass");
    glass.style.display = 'none'

    /* Вставить увеличительное стекло: */
    img.parentElement.insertBefore(glass, img);

    /* Установите свойства фона для стекла лупы: */
    glass.style.backgroundImage = "url('" + img.src + "')";
    glass.style.backgroundRepeat = "no-repeat";
    glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
    bw = 3;
    w = glass.offsetWidth / 2;
    h = glass.offsetHeight / 2;

    /* Выполните функцию, когда кто-то перемещает лупу по изображению: */
    glass.addEventListener("mousemove", moveMagnifier);
    img.addEventListener("mousemove", moveMagnifier);

    /* а также для сенсорных экранов: */
    glass.addEventListener("touchmove", moveMagnifier);
    img.addEventListener("touchmove", moveMagnifier);
    function moveMagnifier(e) {
        var pos, x, y;
        /* Предотвратите любые другие действия, которые могут возникнуть при перемещении по изображению */
        e.preventDefault();
        /* Получить позиции курсора x и y: */
        pos = getCursorPos(e);
        x = pos.x;
        y = pos.y;
        /* Не допускайте, чтобы лупа находилась вне изображения: */
        if (x > img.width - (w / zoom)) { x = img.width - (w / zoom); }
        if (x < w / zoom) { x = w / zoom; }
        if (y > img.height - (h / zoom)) { y = img.height - (h / zoom); }
        if (y < h / zoom) { y = h / zoom; }
        /* Установите положение стекла лупы: */
        glass.style.left = (x - w) + "px";
        glass.style.top = (y - h) + "px";
        /* Покажите, что такое увеличительное стекло "смотреть": */
        glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
    }

    function getCursorPos(e) {
        var a, x = 0, y = 0;
        e = e || window.event;
        /* Получить x и y позиции изображения: */
        a = img.getBoundingClientRect();
        /* Вычислите координаты курсора x и y относительно изображения: */
        x = e.pageX - a.left;
        y = e.pageY - a.top;
        /* Consider any page scrolling: */
        x = x - window.pageXOffset;
        y = y - window.pageYOffset;
        return { x: x, y: y };
    }
}

search_img.onclick = () => {
    let glass_window = document.querySelector('.img-magnifier-glass')

    glass_window.style.display = 'flex'
}



