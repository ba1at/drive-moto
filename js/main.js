let arrow = document.querySelectorAll('#arrow')
let our__advice = document.querySelectorAll('.our__advice .category___selection p')
let main = []
let res_categoria_2 = document.querySelectorAll('.res_categoria_2')

let main_db; // главный массив
let secondly_db;

if (!localStorage.main_db) localStorage.main_db = JSON.stringify(arr)
else main_db = JSON.parse(localStorage.main_db)

if (!localStorage.secondly_db) localStorage.secondly_db = JSON.stringify(anasynk)
else secondly_db = JSON.parse(localStorage.secondly_db)

console.log(main_db);

const RELOAD_POPULAR_START = (i) => {
    let block_push = i.parentNode.parentNode.children[2].children[1]
    let block_push_2 = i.parentNode.parentNode.children[3].children[1]
    let number = i.getAttribute('hello_wepro')
    if (secondly_db[number].length == 1) {
        if (window.screen.width <= '520') {
            i.parentNode.parentNode.children[3].style.display = 'none'
        } else {
            i.parentNode.parentNode.children[2].style.display = 'none'
        }
        i.parentNode.parentNode.children[4].style.display = 'block'
    } else {
        if (window.screen.width <= '520') {
            i.parentNode.parentNode.children[3].style.display = 'flex'
        } else {
            i.parentNode.parentNode.children[2].style.display = 'flex'
        }
        i.parentNode.parentNode.children[4].style.display = 'none'

        block_push.innerText = ''
        block_push_2.innerHTML = ''

        Get_masiv(number, block_push)
        Get_masiv(number, block_push_2)
        scrol()
        Scrop_2()
        for (let item of arrow) {
            item.onclick = () => {
                block_push.innerText = ''
                block_push_2.innerText = ''
                Get_masiv(number, block_push)
                Get_masiv(number, block_push_2)
                scrol()
                Scrop_2()
                second()
                first()
                bebra(number)
            }
        }
    }
}

const RELOAD_POPULAR = (elem, append) => {
    let block = document.createElement('div')
    let img_sale = document.createElement('img')
    let like = document.createElement('img')
    let img = document.createElement('img')
    let name_product = document.createElement('p')
    let price = document.createElement('p')
    let kart = document.createElement('img')
    let btn = document.createElement('button')
    if (elem.code) block.setAttribute('cod', elem.cod)
    block.setAttribute('brend', elem.brend)

    block.classList.add('popular_block')
    img_sale.classList.add('img_sale')
    like.classList.add('like_block')
    img.classList.add('img_tavar')
    name_product.classList.add('name_product')
    price.classList.add('price_block')
    kart.classList.add('kart_img')
    btn.classList.add('btvac')

    block.setAttribute('oneOrtwo', elem.oneOrtwo)

    img_sale.src = './img/sale_img.svg'
    
    img.src = elem.img
    name_product.innerText = elem.name
    kart.src = './img/kart_img.svg'
    btn.innerText = 'посмотреть товар'

    block.setAttribute('atribute', elem.atribute)

    block.append(img_sale, like, img, name_product, price, kart, btn)
    append.append(block)

    if (elem.sale == true) {
        img_sale.style.display = 'block'
    } else {
        img_sale.style.display = 'none'
    }

    if (elem.count == 0) {
        price.innerText = 'нет в наличии'
        price.style.fontSize = '20px'
        price.style.marginTop = '0px'
    } else {
        price.innerText = elem.price + ' ₽'
    }
    
    if (elem.like) {
        console.log(block);
        block.children[1].src = './img/like.png'
        block.children[1].style.width = '22px'
        block.children[1].style.height = '24px'
    } else {
        block.children[1].src = './img/like.svg'
    }

    like.onclick = () => like_func(elem, block)
    kart.onclick = () => kart_func(elem, block)
}

const like_func = (param, bl) => {
    param.like = !param.like
    if (param.like) {
        bl.children[1].src = './img/like.png'
        bl.children[1].style.width = '22px'
        bl.children[1].style.height = '24px'
    } else {
        bl.children[1].src = './img/like.svg'
    }

    let el = main_db[bl.getAttribute("atribute")].filter(item => item.id == param.id)[0]
    localStorage.main_db = JSON.stringify(main_db)

}

const kart_func = (param, bl) => {
    param.kart = !param.kart
    

    let el = main_db[bl.getAttribute("atribute")].filter(item => item.id == param.id)[0]
    localStorage.main_db = JSON.stringify(main_db)

}

let result_search = document.querySelector('.result_search')

for (let item of main_db) {
    for (let i of item) {
        RELOAD_POPULAR(i, result_search)
    }
}

for (let item of secondly_db) {
    for (let i of item) {
        if (i.sale == true) {
            main.push(i)
        } else { }
    }
}

let sayd_b = document.querySelector('.sayd_b')
for (let item of main) {
    RELOAD_POPULAR(item, sayd_b)
}

const Get_masiv = (a, b) => {
    let math = Math.random().toString().slice(2, 3)

    for (let item = 0; item <= 3; item++) {
        if (math >= 1) {

        } else {
            math = '5'
        }
        math = math - 1

        RELOAD_POPULAR(secondly_db[a][math], b)
    }
}

let res_categoria = document.querySelectorAll('.res_categoria')
for (let item of res_categoria) {
    Get_masiv(0, item)
}

for (let item of res_categoria_2) {
    Get_masiv(0, item)
}

let counter = 1
const scrol = () => {
    let top_side_2 = document.querySelector('.res_categoria_2')
    let images = document.querySelectorAll('.popular .res_categoria_2 div')

    let status = true

    const NEXT_SLIDE = (isNext) => {
        // Удаляем класс у всех элементов
        for (let item of images) {
            item.style.display = 'none'
        }


        if (isNext) {
            counter++

            // Включаем бесконечную прокрутку
            if (counter >= images.length) {
                // Убрать класс у последнего
                counter = 0
            }
        } else {
            counter--

            if (counter < 0) counter = images.length
        }

        images[counter].style.display = 'block'
    }

    let slider = setInterval(() => {
        if (status === true) {
            NEXT_SLIDE(true)
        }
    }, 5000)

    // Pause slider
    top_side_2.onmouseenter = () => status = false

    // Continue slider
    top_side_2.onmouseleave = () => status = true

    let popular_arrow = document.querySelectorAll('.popular .res_cat #arrow')

    for (let item of popular_arrow) {
        item.onclick = () => {
            counter++
            NEXT_SLIDE(true)
        }
    }
}

let counter_2 = 0
const Scrop_2 = () => {
    let top_side_4 = document.querySelector('.our__advice .res_categoria_2')
    let images_2 = document.querySelectorAll('.our__advice .res_categoria_2 div')

    let status = true

    const NEXT_SLIDE = (isNext) => {
        // Удаляем класс у всех элементов
        for (let item of images_2) {
            item.style.display = 'none'
        }

        if (isNext) {
            counter_2++

            // Включаем бесконечную прокрутку
            if (counter_2 >= images_2.length) {
                // Убрать класс у последнего
                counter_2 = 0
            }
        } else {
            counter_2--
            if (counter_2 < 0) counter_2 = images_2.length - 1
        }

        images_2[counter_2].style.display = 'block'
    }

    let slider = setInterval(() => {
        if (status === true) {
            NEXT_SLIDE(true)
        }
    }, 5000)

    // Pause slider
    top_side_4.onmouseenter = () => status = false

    // Continue slider
    top_side_4.onmouseleave = () => status = true

    let popular_arrow = document.querySelectorAll('.our__advice .res_cat #arrow')

    for (let item of popular_arrow) {
        item.onclick = () => {
            counter_2++
            NEXT_SLIDE(true)
        }
    }
}
scrol()
Scrop_2()

// Переключение категорий в поиске
let katekorii_btn = document.querySelectorAll('.katekorii_btn')

for (let item of katekorii_btn) {
    item.onclick = () => {
        for (let i of katekorii_btn) {
            i.classList.remove('active_katekorii')
            i.removeAttribute('active')
        }
        item.classList.add('active_katekorii')
        item.setAttribute('active', true)
        search_inp(item)
    }
}

let cals = document.querySelector('.sayd_b div')
let cals_all = document.querySelectorAll('.sayd_b div')

for (let item of cals_all) {
    item.style.display = 'none'
}
cals.style.display = 'block'

const first = () => {
    let first = document.querySelector('.popular .res_categoria_2 div')
    let all = document.querySelectorAll('.popular .res_categoria_2 div')

    for (let item of all) {
        item.style.display = 'none'
    }
    first.style.display = 'block'
    counter = 0
}

const second = () => {
    let first_2 = document.querySelector('.our__advice .res_categoria_2 div')
    let all_2 = document.querySelectorAll('.our__advice .res_categoria_2 div')

    for (let item of all_2) {
        item.style.display = 'none'
    }
    first_2.style.display = 'block'
    counter_2 = 0
}

first()
second()

let our__advice_arrow_2 = document.querySelectorAll('.popular .res_cat_1 #arrow')

let pop_cat = document.querySelectorAll('.popular .category___selection p')

for (let item of our__advice_arrow_2) {
    item.onclick = () => {
        item.parentNode.children[1].innerHTML = ''
        Get_masiv(0, item.parentNode.children[1])
        first()
        bebra(0)
    }
}

for (let item of pop_cat) {
    item.onclick = () => {
        for (let i of pop_cat) {
            i.classList.remove('active_category_selection')
        }
        item.classList.add('active_category_selection')

        RELOAD_POPULAR_START(item)
        first()
        bebra(item.getAttribute('hello_wepro'))
    }
}

let our__advice_arrow = document.querySelectorAll('.our__advice .res_cat_1 #arrow')
let pop_cat_2 = document.querySelectorAll('.our__advice .category___selection p')


for (let item of our__advice_arrow) {
    item.onclick = () => {
        item.parentNode.children[1].innerHTML = ''
        Get_masiv(0, item.parentNode.children[1])
        second()
        bebra(0)
    }
}

for (let item of pop_cat_2) {
    item.onclick = () => {
        for (let i of pop_cat_2) {
            i.classList.remove('active_category_selection')
        }
        item.classList.add('active_category_selection')

        RELOAD_POPULAR_START(item)
        second()
        bebra(item.getAttribute('hello_wepro'))
    }
}

const bebra = (kto) => {
    let res_categoriaBlock = document.querySelectorAll('.res_categoria div button')
    let res_categoriaBlock_2 = document.querySelectorAll('.res_categoria_2 div button')

    for (let item of res_categoriaBlock) {
        item.onclick = () => {
            id(item.parentNode.id, kto)
        }
    }

    for (let item of res_categoriaBlock_2) {
        item.onclick = () => {
            id(item.parentNode.id, kto)
        }
    }

    function id(id, sec) {
        let find = secondly_db[sec].filter(item => item.id == id)[0]
        localStorage.setItem('arr', JSON.stringify(find))
        window.location.href = './elem.html'
    }
}

function id3(id, sec, thred) {
    let f = thred[sec].filter(item => item.id == id)[0]
    localStorage.setItem('arr', JSON.stringify(f))
    window.location.href = './elem.html'
}

bebra(0)

let sayd_bDiv = document.querySelectorAll('.sayd_b .popular_block .btvac')


for (let item of sayd_bDiv) {
    item.onclick = () => {
        let id = item.parentNode.id
        let fi = secondly_db[item.parentNode.getAttribute('atribute')].filter(item => item.id == id)[0]
        localStorage.setItem('arr', JSON.stringify(fi))
        window.location.href = './elem.html'
    }
}

let result_searchDiv = document.querySelectorAll('.result_search .popular_block .btvac')

for (let item of result_searchDiv) {
    item.onclick = () => {
        id3(item.parentNode.id, item.parentNode.getAttribute('atribute'), main_db)
    }
}

let elasticItems_1 = document.querySelectorAll('.result_search .popular_block') // Элементы по которым мы будем делать поиск

for (let item of elasticItems_1) {
    item.classList.add('hide')
}

function search_inp(params) {
    let input = document.querySelector('.input_search input') // Наш input
    input.oninput = function () {
        let elasticItems = document.querySelectorAll('.result_search .popular_block') // Элементы по которым мы будем делать поиск

        for (let item of elasticItems) {
            item.classList.add('hide')
        }

        let val = this.value.trim();

        if (params.hasAttribute('cod')) {
            if (val != '') {
                elasticItems.forEach(function (elem) {
                    if (elem.getAttribute('cod').search(val) == -1) {
                        elem.classList.add('hide')
                    }
                    else {
                        elem.classList.remove('hide')
                    }
                });
            }
            else {
                elasticItems.forEach(function (elem) {
                    elem.classList.remove('hide')
                });
            }
        } else if (params.hasAttribute('brend')) {
            if (val != '') {
                elasticItems.forEach(function (elem) {
                    if (elem.getAttribute('brend').search(val) == -1) {
                        elem.classList.add('hide')
                    }
                    else {
                        elem.classList.remove('hide')
                    }
                });
            }
            else {
                elasticItems.forEach(function (elem) {
                    elem.classList.remove('hide')
                });
            }
        } else {
            if (val != '') {
                elasticItems.forEach(function (elem) {
                    if (elem.innerText.search(val) == -1) {
                        elem.classList.add('hide')
                    }
                    else {
                        elem.classList.remove('hide')
                    }
                });
            }
            else {
                elasticItems.forEach(function (elem) {
                    elem.classList.remove('hide')
                });
            }
        }

    }
}

search_inp(katekorii_btn[0])


