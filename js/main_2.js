let span_puti = document.querySelector('.puti span')
let tytle = document.querySelector('.tytle')
let sayd_a = document.querySelector('.sayd_a')
let aside = document.querySelector('aside')

span_puti.innerText = localStorage.getItem('text')
tytle.innerText = localStorage.getItem('text')

let title_body = document.querySelector('body')

title_body.parentNode.children[0].children[7].innerText = localStorage.getItem('text')

let all_cat = document.querySelector('.all_cat')


const RELOAD_POPULAR = (elem, append) => {
    let block = document.createElement('div')
    let img_sale = document.createElement('img')
    let like = document.createElement('img')
    let img = document.createElement('img')
    let name_product = document.createElement('p')
    let price = document.createElement('p')
    let kart = document.createElement('img')
    let btn = document.createElement('button')

    block.setAttribute('id', elem.id)
    block.setAttribute('count', elem.count)
    block.setAttribute('data', elem.year)

    block.classList.add('popular_block')
    img_sale.classList.add('img_sale')
    like.classList.add('like_block')
    img.classList.add('img_tavar')
    name_product.classList.add('name_product')
    price.classList.add('price_block')
    kart.classList.add('kart_img')
    btn.classList.add('btvac')

    like.onclick = () => like_func(elem)
    kart.onclick = () => kart_func(elem)

    let big = elem.max

    if (elem.max == true) {
        block.setAttribute('its_max', true)
    } else { }

    img_sale.src = './img/sale_img.svg'
    like.src = './img/like.svg'
    img.src = elem.img
    name_product.innerText = elem.name
    kart.src = './img/kart_img.svg'
    btn.innerText = 'посмотреть товар'

    block.append(img_sale, like, img, name_product, price, kart, btn)
    append.append(block)

    const kart_func = (param, bl) => {
        param.kart = !param.kart

        let el = main_db[bl.getAttribute("atribute")].filter(item => item.id == param.id)[0]
        localStorage.main_db = JSON.stringify(main_db)
    }


    const like_func = (param) => {
        param.like = false
        localStorage.main_db = main_db[bl.getAttribute("atribute")].filter(item => item.id == param.id)[0] = el

        RELOAD_POPULAR(param, bab)
    }

    if (elem.sale == true) {
        img_sale.style.display = 'block'
        block.setAttribute('salle', true)
    } else {
        img_sale.style.display = 'none'
    }

    if (elem.count == 0) {
        price.innerText = 'нет в наличии'
        price.style.fontSize = '20px'
        price.style.marginTop = '0px'
    } else {
        price.innerText = elem.price
    }
}

for (let item of arr[localStorage.getItem('atribute')]) {
    RELOAD_POPULAR(item, all_cat)
}

let rangeInput = document.querySelectorAll('.range-input input'),
    progress = document.querySelector('.range .progress'),
    j_sB = document.querySelectorAll('.j_sB p'),
    priceGap = 1000;

rangeInput.forEach(input => {
    input.addEventListener('input', e => {
        let minVal = parseInt(rangeInput[0].value),
            maxVal = parseInt(rangeInput[1].value);

        if (maxVal - minVal < priceGap) {
            if (e.target.className === 'range-min') {
                rangeInput[0].value = maxVal - priceGap;
            } else {
                rangeInput[1].value = minVal + priceGap;
            }
        } else {
            progress.style.left = (minVal / rangeInput[0].max) * 100 + '%';
            j_sB[0].innerText = 'от ' + minVal
            j_sB[1].innerText = 'до ' + maxVal
            progress.style.right = 100 - (maxVal / rangeInput[1].max) * 100 + '%';
        }
    })
})


let l_s = document.querySelectorAll('.mosh select')
let result = document.querySelector('.result')
let lasst = document.querySelector('.last div select')

for (let item of arr[localStorage.getItem('atribute')]) {
    let option = document.createElement('option'),
        option_2 = document.createElement('option'),
        option_3 = document.createElement('option'),
        p = document.createElement('p');

    option.innerHTML = item.power_l_s
    option_2.innerHTML = item.maks_km_h
    option_3.innerHTML = item.country

    p.innerText = item.name
    p.classList.add('hide')

    l_s[0].append(option)
    l_s[1].append(option_2)
    lasst.append(option_3)
    result.append(p)
}


// let input = document.querySelector('.search_div input') // Наш input

// input.oninput = function () {
//     let val = this.value.trim();
//     let elasticItems = document.querySelectorAll('.result p') // Элементы по которым мы будем делать поиск
//     if (val != '') {
//         elasticItems.forEach(function (elem) {
//             if (elem.innerText.search(val) == -1) {
//                 elem.classList.add('hide')
//             }
//             else {
//                 elem.classList.remove('hide')
//             }
//         });
//     }
//     else {
//         elasticItems.forEach(function (elem) {
//             elem.classList.remove('hide')
//         });
//     }
// }

let sayd_b_img = document.querySelectorAll('.sayd_b img')
let pop_block = document.querySelectorAll('.popular_block')

sayd_b_img[0].onclick = () => {
    all_cat.style.display = 'grid'
    sayd_b_img[0].boxShadow = '3px 3px 20px rgba(50, 50, 50, 0.25)'
    all_cat.style.width = 'auto'
    for (let item of pop_block) {
        item.style.marginTop = '0'
    }
}

sayd_b_img[1].onclick = () => {
    all_cat.style.display = 'block'
    for (let item of pop_block) {
        item.style.marginTop = '20px'
        pop_block[0].style.marginTop = '0'
        all_cat.style.width = '100%'
        item.style.width = '100%'
    }
}

let sayd_b_btn = document.querySelector('.sayd_b button')

sayd_b_btn.onclick = () => {
    if (sayd_b_btn.innerText == 'Открыть aside') {
        sayd_b_btn.parentNode.parentNode.parentNode.parentNode.parentNode.children[3].children[0].style.flexDirection = 'column'
        aside.style.display = 'block'
        all_cat.style.marginTop = '15px'
        sayd_b_btn.innerHTML = 'Закрыть aside'
    } else if (sayd_b_btn.innerText == 'Закрыть aside') {
        aside.style.display = 'none'
        all_cat.style.marginTop = '0px'
        sayd_b_btn.innerHTML = 'Открыть aside'
    }
}

let res_categoria_2 = document.querySelector('.res_categoria_2')

for (let item of anasynk[0]) {
    RELOAD_POPULAR(item, res_categoria_2)
}


let category___selection_p = document.querySelectorAll('.category___selection p')
let if_none = document.querySelector('.if_none')

for (let i of category___selection_p) {
    i.onclick = () => {
        for (let item of category___selection_p) {
            item.classList.remove('active_category_selection')
        }
        i.classList.add('active_category_selection')
        if (anasynk[i.getAttribute('hello_wepro')].length <= 1) {
            if_none.style.display = 'flex'
            res_categoria_2.style.display = 'none'
        } else {
            if_none.style.display = 'none'
            res_categoria_2.style.display = 'flex'
            res_categoria_2.innerHTML = ''
            for (let elem of anasynk[i.getAttribute('hello_wepro')]) {
                RELOAD_POPULAR(elem, res_categoria_2)
                scrol()

                let res_categoriaBlock_2 = document.querySelectorAll('.res_categoria_2 .popular_block .btvac')

                for (let el of res_categoriaBlock_2) {
                    el.onclick = () => {
                        id2(el.parentNode.id, i.parentNode.getAttribute('hello_wepro'))
                    }
                }
            }
        }
    }
}

const scrol = () => {
    let images = document.querySelectorAll('.res_categoria_2 div')
    for (let item of images) {
        item.style.display = 'none'
        images[0].style.display = 'block'
    }
    let top_side_2 = document.querySelector('.res_categoria_2')
    let counter = 0

    let status = true

    const NEXT_SLIDE = (isNext) => {
        // Удаляем класс у всех элементов
        for (let item of images) {
            item.style.display = 'none'
        }

        if (isNext) {
            counter++

            // Включаем бесконечную прокрутку
            if (counter >= images.length) {
                // Убрать класс у последнего
                counter = 0
            }
        } else {
            counter--

            if (counter < 0) counter = images.length - 1
        }

        images[counter].style.display = 'block'
    }

    let slider = setInterval(() => {
        if (status === true) {
            NEXT_SLIDE(true)
        }
    }, 2000)

    // Pause slider
    top_side_2.onmouseenter = () => status = false

    // Continue slider
    top_side_2.onmouseleave = () => status = true
}
scrol()

let tytleWithImg = document.querySelectorAll('.tytle_with_img')

for (let item of tytleWithImg) {
    item.onclick = () => {
        if (item.hasAttribute('active') == true) {
            item.parentNode.style.height = 'auto'
            item.parentNode.style.overflow = 'hidden'
            item.children[0].style.transform = 'rotate(180deg)'
            item.removeAttribute('active')
        } else {
            item.parentNode.style.height = '22px'
            item.parentNode.style.overflow = 'hidden'
            item.children[0].style.transform = 'rotate(0deg)'
            item.setAttribute('active', true)
        }
    }
}

let all_catDiv = document.querySelectorAll('.all_cat .popular_block .btvac')

for (let item of all_catDiv) {
    item.onclick = () => {
        id(item.parentNode.id)
    }
}

let res_categoriaBlock = document.querySelectorAll('.res_categoria_2 .popular_block .btvac')

for (let item of res_categoriaBlock) {
    item.onclick = () => {
        id2(item.parentNode.id, 0)
    }
}

function id2(id, sec) {
    let fi = anasynk[sec].filter(item => item.id == id)[0]
    localStorage.setItem('arr', JSON.stringify(fi))
    window.location.href = './elem.html'
}

function id(id) {
    let find = arr[localStorage.getItem('atribute')].filter(item => item.id == id)[0]
    localStorage.setItem('arr', JSON.stringify(find))
    window.location.href = './elem.html'
}

let inputHavenotHave = document.querySelectorAll('input[name="radio"]')

for (let item of inputHavenotHave) {
    item.onclick = () => {
        if (item.getAttribute('id') == 'have') {
            for (let el of all_cat.children) {
                el.style.display = 'block'
            }
        }
        else {
            for (let el of all_cat.children) {
                el.style.display = 'none'
                if (el.getAttribute('count') <= 0) {
                    el.style.display = 'block'
                }
            }
        }
    }
}

let inputNewAktsii = document.querySelectorAll('input[name="date"]')

for (let item of inputNewAktsii) {
    item.onclick = () => {
        if (item.getAttribute('id') == 'all') {
            for (let el of all_cat.children) {
                el.style.display = 'block'
            }
        } else if (item.getAttribute('id') == 'new') {
            for (let el of all_cat.children) {
                el.style.display = 'none'
                if (el.getAttribute('data') >= 2021) {
                    el.style.display = 'block'
                }
            }
        } else {
            for (let el of all_cat.children) {
                el.style.display = 'none'
                if (el.hasAttribute('salle')) {
                    el.style.display = 'block'
                }
            }
        }
    }
}

let moshSelect = document.querySelectorAll('.mosh select')
let mas = []
let horse_pow = document.querySelector("select[name='l_s']")

let obj_properties = {
    power_l_s: 0,
    maks_km_h: 0,
    sale: 0,
    country: String,
    price: 0
}

horse_pow.onchange = () => {
    let el
    if (event.target.children[0].getAttribute("value") == 123) {
        for (let it of arr[localStorage.getItem('atribute')]) {
            all_cat.innerHTML = ""
            RELOAD_POPULAR(it, all_cat)
        }
    }

    for (let it of arr[localStorage.getItem('atribute')]) {
        if (it.power_l_s == event.target.value) {

            all_cat.innerHTML = ""

            obj_properties.power_l_s = it.power_l_s
            check_filter(obj_properties)

            RELOAD_POPULAR(it, all_cat)
            mas.push(it)
            return
        }
    }
}

let max_speed = document.querySelector("select[name='max_speed']")

max_speed.onchange = () => {
    let el
    if (event.target.children[0].getAttribute("value") == 123) {
        for (let it of arr[localStorage.getItem('atribute')]) {
            all_cat.innerHTML = ""
            RELOAD_POPULAR(it, all_cat)
        }
    }

    for (let it of arr[localStorage.getItem('atribute')]) {
        if (it.maks_km_h == event.target.value) {


            all_cat.innerHTML = ""
            obj_properties.maks_km_h = it.maks_km_h
            check_filter(obj_properties)
            RELOAD_POPULAR(it, all_cat)
            mas.push(it)
            return
        }
    }
}
let model_find = document.querySelector("#model_find")

model_find.onkeyup = () => {
    all_cat.innerHTML = ""
    let word = event.target.value.toLowerCase().trim()
    if (word.length >= 3) {
        for (let item of arr[localStorage.getItem('atribute')]) {
            if (item.name.toLowerCase().trim().includes(word)) {
                obj_properties.name = it.name
                all_cat.innerHTML = ""
                check_filter(obj_properties)

                RELOAD_POPULAR(item, all_cat)
            }
        }
    } else {
        for (let it of arr[localStorage.getItem('atribute')]) {
            RELOAD_POPULAR(it, all_cat)
        }
    }
}

let range_ = document.querySelectorAll('input[type="range"]')
let t_arr = []
for (let item of range_) {
    let inp_1 = range_[0].value,
        inp_2 = range_[1].value


    item.onchange = () => {
        inp_1 = range_[0].value
        inp_2 = range_[1].value
        for (let it of arr[localStorage.getItem('atribute')]) {
            let prc_it = it.price.replaceAll(" ", "")

            if (prc_it >= inp_1 && prc_it <= inp_2) {
                all_cat.innerHTML = ""
                t_arr.push(it)
                obj_properties.price = it.price
                check_filter(obj_properties)

                for (let item of t_arr) {
                    RELOAD_POPULAR(item, all_cat)
                }
            } else {
                t_arr.splice(t_arr.indexOf(it), 1)
                all_cat.innerHTML = ""
                RELOAD_POPULAR(it, all_cat)
            }
        }
    }
}

let country = document.getElementById('country')

country.onchange = () => {
    let el
    if (event.target.children[0].getAttribute("value") == 123) {
        for (let it of arr[localStorage.getItem('atribute')]) {
            RELOAD_POPULAR(it, all_cat)
        }
    }

    for (let it of arr[localStorage.getItem('atribute')]) {
        if (it.country.toLowerCase().trim().includes(event.target.value.toLowerCase().trim())) {
            all_cat.innerHTML = ""
            obj_properties.country = it.country
            check_filter(obj_properties)
            RELOAD_POPULAR(it, all_cat)
        } else {
            RELOAD_POPULAR(it, all_cat)
        }
    }
}

const check_filter = (obj) => {
    let mas_a = arr[localStorage.getItem('atribute')]
    const result = mas_a.filter(car =>
        Object.keys(obj).every(key =>
            mas[key] === obj[key])
    );
    console.log(result);
}

