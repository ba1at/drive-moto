let footer = document.createElement('footer')
let styleFooter = document.createElement('style')

styleFooter.innerHTML = `

footer {
    margin-top: 40px;
    width: 100%;
    height: auto;
    background: #F0F0F4;
    padding: 25px 0;
}

.space_between {
    display: flex;
    justify-content: space-between;
}

.tytle_email {
    font-weight: 700;
    font-size: 14px;
    line-height: 17px;
    color: #2F3035;
}

.email {
    display: flex;
    margin-top: 30px;
}

.email input {
    width: 180px;
    height: 33px;
    background: #FFFFFF;
    border: none;
    padding-left: 12px;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    outline: none;
}

.email input::placeholder {
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    color: #C4C4C4;
}

.email button {
    width: 109px;
    background: #1C62CD;
    border: none;
    font-weight: 600;
    font-size: 11px;
    line-height: 13px;
    letter-spacing: 0.12em;
    text-transform: uppercase;
    color: #FFFFFF;
}

.t {
    font-weight: 700;
    font-size: 14px;
    line-height: 17px;
    color: #2F3035;
}

.desc {
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
    color: #2F3035;
    cursor: pointer;
    margin-top: 5px;
}

.sayd_footer_last{
    display: flex;
    align-items: center;
}

.sayd_footer_last img{
    width: 30px;
    height: 30px;
    margin-left: 20px;
    cursor: pointer;
}

.sayd_footer_last img:nth-child(1){
    margin-left: 0;
}

.img_open{
    display: none;
    transition: 300ms;
}

.rotate{
}

@media only screen and (max-width: 800px){
    footer{
        margin-top: 30px; 
        padding-bottom: 15px;
        background-color: #F9F9FC;
    }
    
    .space_between{
        display: block;
    }
    
    .space_between .sayd_footer:nth-child(1){
        padding-top: 15px;
    }
    
    .sayd_footer, .sayd_footer_last{
        padding: 0 15px;
    }

    .email{
        margin-top: 20px;
    }
    
    .email input{
        width: 100%;
    }

    .tytle_email{
        text-align: center;
    }

    .sayd_footer_last{
        margin-top: 30px;
        justify-content: center;
    }

    .sayd_footer:nth-child(2){
        margin-top: 30px;
    }
    
    .sayd_footer:nth-child(2), .sayd_footer:nth-child(3){
        padding: 15px;
        border-bottom: 2px solid #F0F0F4;
        border-top: 2px solid #F0F0F4;
        position: relative;
        height: 50px;
        display: flex;
        flex-direction: column;
        overflow: hidden;
        transition: 300ms;
    }

    .sayd_footer:nth-child(3){
        border-top: none;
    }

    .t{
        font-weight: 500;
        font-size: 14px;
        line-height: 18px;
        letter-spacing: -0.0861538px;
        color: #7F7F7F;
        padding-bottom: 25px;
    }

    .img_open{
        display: block;
        position: absolute;
        right: 15px;
        top: 50%;
        transform: translate(0, -50%);
    }
}
`

footer.innerHTML = `
<div class="def_width">
    <div class="space_between">
        <div class="sayd_footer">
        <p class="tytle_email">Подпишитесь на нашу рассылку <br> и узнавайте о акция быстрее</p>
        
        <div class="email">
                <input type="text" placeholder="Введите ваш e-mail:"><button>Отправить</button>
            </div>
        </div>

        <div class="sayd_footer">
            <p class="t">Информация</p>
            <p class="desc">О компании</p>
            <p class="desc">Контакты</p>
            <p class="desc">Акции</p>
            <p class="desc">Магазины</p>
            <img src="./img/Foward.svg" class="img_open">
        </div>

        <div class="sayd_footer">
            <p class="t">Интернет-магазин</p>
            <p class="desc">Доставка и самовывоз</p>
            <p class="desc">Оплата</p>
            <p class="desc">Возврат-обмен</p>
            <p class="desc">Новости</p>
            <img src="./img/Foward.svg" class="img_open">
        </div>

        <div class="sayd_footer_last">
            <img src="./img/instagram-sketched (1) 1.svg" alt="">
            <img src="./img/vk 1.svg" alt="">
            <img src="./img/facebook 1.svg" alt="">
            <img src="./img/youtube 1.svg" alt="" srcset="">
            </div>
    </div>
</div>
`

document.querySelector('body').append(footer, styleFooter)

let img_open = document.querySelectorAll('.img_open')

for (let item of img_open) {

    item.onclick = () => {
        if (item.hasAttribute('onlick') == true) {
            item.parentNode.style.height = '50px'
            item.removeAttribute('onlick')
            item.style.transform = 'rotate(0deg)'
            item.classList.remove('rotate')
        } else {
            item.parentNode.style.height = 'auto'
            item.classList.add('rotate')
            item.style.transform = 'rotate(180deg)'
            item.setAttribute('onlick', true)
        }
    }
}




let header = document.createElement('header')
let headerStyle = document.createElement('style')

headerStyle.innerHTML = `
header{
    margin-top: 25px;
}

.top_header {
    display: flex;
    justify-content: space-between;
    position: relative;
}

.left_header {
    height: 70px;
    display: flex;
    align-items: center;
}

.right_header {
    display: flex;
    align-items: center;
}

.left_header p {
    cursor: pointer;
    margin-left: 25px;
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    color: #2F3035;
}

.left_header p:nth-child(1) {
    margin-left: 0;
}

.logo {
    cursor: pointer;
}

.place {
    font-weight: 500;
    font-size: 16px;
    line-height: 24px;
    color: #2F3035;
    display: flex;
    align-items: center;
}

.place img {
    width: 20px;
    height: 20px;
}

.capabilities {
    margin-left: 50px;
    display: flex;
}

.capabilities img {
    cursor: pointer;
    margin-left: 10px;
}

.capabilities img:nth-child(1) {
    margin-left: 0px;
}

.bottom_header {
    width: 100%;
    height: 53px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: #F0F0F4;
    margin-top: 20px;
}

.bottom_header p {
    display: flex;
    align-items: center;
    height: 100%;
    cursor: pointer;
    font-weight: 400;
    font-size: 20px;
    line-height: 24px;
    text-transform: capitalize;
    color: #2F3035;
    transition: 200ms;
}

.active_header_link{
    border-bottom: 2px solid #1C62CD;
}

.show_in_phone{
    display: none;
    align-items: center;
}

.modal{
    position: fixed;
    left: 10000%;
    top: 10000%;
    right: 0;
    bottom: 0;
    background-color: white;
    z-index: 1000000;
    transition: 300ms;
}

.window{
    width: 70%;
    height: 100%;
    background: #FFFFFF;
    box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.15);
    display: flex;
    flex-direction: column;
    justify-content: center;
    position: relative;
}

.modal p {
    margin-top: 20px;
    padding-left: 10px;
    width: 100%;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    font-weight: 400;
    font-size: 20px;
    line-height: 24px;
    color: #2F3035;
}

.modal p:nth-child(1){
    margin-top: 0px;
}

.close{
    width: 30px;
    height: 30px;
    position: absolute;
    right: 15px;
    top: 15px;
}

@media only screen and (max-width: 900px){
    .none_dis{
        display: none;
    }

    header{
        margin-top: 10px;
    }

    .top_header{
        height: 60px;
        position: relative;
    }
    
    .logo{
        width: 65px;
        height: 61px;
        position: absolute;
        left: 50%;
        top: 0;
        transform: translate(-50%, 0);
    }

    .show_in_phone{
        cursor: pointer;
        display: flex;
    }
}

`

header.innerHTML = `
<div class="def_width">
<div class="top_header">
    <div class="left_header none_dis">
        <p>Магазины</p>
        <p>Акции</p>
        <p>Доставка и оплата</p>
    </div>
    
    <div class="show_in_phone">
    <img src="./img/menu.svg">
    </div>

    <a href="./index.html">
    <img src="./img/logo.svg" class="logo" alt="">
    </a>

    <div class="right_header">
        <p class="place none_dis"><img src="./img/place.svg" alt="">Москва,  ул. Науки  25</p>

        <div class="capabilities">
            <img src="./img/like.svg" class="like_img" alt="">
            <img src="./img/person.svg" alt="">
            <img src="./img/cart.svg" alt="">
        </div>
    </div>
</div>

<div class="bottom_header none_dis">
    <p kateqoria="0">Квадроциклы</p>
    <p kateqoria="1">Катера</p>
    <p kateqoria="2">Гидроциклы</p>
    <p kateqoria="3">Лодки</p>
    <p kateqoria="4">Вездеходы</p>
    <p kateqoria="5">Снегоходы</p>
    <p kateqoria="6">Двигатели</p>
</div>
</div>

<div class="modal">
    <div class="window">
        <img src="./img/close_FILL0_wght400_GRAD0_opsz48.svg" class="close">
        <p><img src="./img/like.svg" style="width: 20px; height: 20px"> Избранное</p>
        <p><img src="./img/cart.svg" style="width: 20px; height: 20px"> Корзина</p>
        <p kateqoria="0" class="aass">Квадроциклы</p>
        <p kateqoria="1" class="aass">Катера</p>
        <p kateqoria="2" class="aass">Гидроциклы</p>
        <p kateqoria="3" class="aass">Лодки</p>
        <p kateqoria="4" class="aass">Вездеходы</p>
        <p kateqoria="5" class="aass">Снегоходы</p>
        <p kateqoria="6" class="aass">Двигатели</p>
    </div>
</div>
`

document.querySelector('body').prepend(header, headerStyle)

let show_in_phone = document.querySelector('.show_in_phone'),
    modal = document.querySelector('.modal'),
    close_img = document.querySelector('.close'),
    like_img = document.querySelector('.like_img');

show_in_phone.onclick = () => {
    modal.style.top = '0'
    modal.style.left = '0'

    close_img.onclick = () => {
        modal.style.top = '10000%'
        modal.style.left = '10000%'
    }
}

like_img.onclick = () => {
    window.location.href = './likeWindow.html'
}

let window_p = document.querySelectorAll('.window p')

window_p[0].onclick = () => {
    window.location.href = './likeWindow.html'
}